
module.exports = {
  entry: './js/main.js',
  output: {
    path: __dirname,
    filename: './dist/common.js'
  },

  resolve: {
    extensions: ['', '.js']
  },
};