var angular = require('angular');

angular.module('booksServices', [require('angular-resource')])

  .factory('Books', function($resource){
    return $resource('https://netology-fbb-store-api.herokuapp.com/book/:bookId', {}, {
      query: {method:'GET', params:{bookId:''}, isArray:true}
    });
  })
  .factory('OrderMethods', function($resource){
    return $resource('https://netology-fbb-store-api.herokuapp.com/order/:methodOrder', {}, {
      getDelivery: {method:'GET', params:{methodOrder:'delivery'}, isArray:true},
      getPayment: {method:'GET', params:{methodOrder:'payment'}, isArray:true},
    });
  })


var bookApp = angular.module('BookApp', ['booksServices', require('angular-sanitize'), require('angular-route')])

bookApp.config(function($routeProvider, $locationProvider){
  $routeProvider
    .when('/', {
      templateUrl:'./view/main.html',
      controller:'BookList',
      controllerAs:'BookList'
    })
    .when('/book/:bookId', {
      templateUrl:'./view/book.html',
      controller:'BookOne',
      controllerAs:'BookOne'
    })
    .when('/about', {
      templateUrl:'./view/about.html'
    })
    .when('/order/:bookId', {
      templateUrl:'./view/order.html',
      controller:'Order',
      controllerAs:'Order'
    })

  $locationProvider.html5Mode(true);
})

bookApp.directive('ngEye', function() {
  function link(scope, element, attrs) {

    var eye = element.children()[0];
    var eyeWidth = eye.clientWidth;
    var eyeHeight = eye.clientHeight;

    var eyeApple = eye.querySelector('.eye-apple');
    var eyeAppleWidth = eyeApple.clientWidth;
    var eyeAppleHeight = eyeApple.clientHeight;

    var centerH = window.innerWidth / 2;
    var centerV = window.innerHeight / 2;

    document.addEventListener('mousemove', function(e) {
      var moveX = -(e.pageX - centerH) / 30;
      var moveY = -(e.pageY - centerV) / 30;

      var top = (eyeHeight / 2) - (eyeAppleHeight / 2) - moveY +'px';
      var left = (eyeWidth / 2) - (eyeAppleWidth / 2) - moveX +'px';
      scope.eyeAppleStyle = { 'top': top, 'left': left }
      scope.$apply();
    })
  }

  return {
    templateUrl: './view/eye.html',
    link: link
  }
})

bookApp.controller('BookList', function(Books, $routeParams) {
  var limitBooks = 4;

  this.books = Books.query();
  this.limitBooks = limitBooks;
  this.showBtn = true;
  this.upLimitBooks = function() {
    this.limitBooks += limitBooks;
    this.showBtn = !(this.limitBooks >= this.books.length);
  }

})

bookApp.controller('BookOne', function(Books, $routeParams) {
  this.book = Books.get({bookId: $routeParams.bookId});
})

bookApp.controller('Order', function(Books, OrderMethods, $routeParams, $http) {

  var deliveryPrice = 0;
  this.book = Books.get({bookId: $routeParams.bookId});
  this.deliveries = OrderMethods.getDelivery();
  this.payments = OrderMethods.getPayment();
  this.needAdress = false;

  this.addressCheck = function(obj) {
    this.needAdress = obj.needAdress;
  }

  this.getTotal = function(id) {
    var price,
        obj = this.deliveries.find(function(el) { return el.id === id });
    
    if(!obj) return 'Рассчитываем цену';
    
    price = this.book.price + obj.price;

    return 'Итого к оплате: ' + price + ' Z';
  }

  this.getDeliveryName = function(obj) {
    var result = '';

    if( obj.price === 0 ) result = obj.name + ' - Бесплатно';
    else result = obj.name + ' - ' + obj.price + ' Z';

    return result;
  }

  this.form = {
    name: '',
    phone: '',
    email: '',
    comment: '',
    delivery: '',
    address: '',
    paymentId: ''
  }

  this.submitOrder = function() {
    this.error = false;

    var config = {
      method: 'POST',
      url: 'https://netology-fbb-store-api.herokuapp.com/order',
      data: {
        book: this.book.id,
        name: this.form.name,
        phone: this.form.phone,
        email: this.form.email,
        comment: this.form.comment,
        delivery: {
          id: this.form.delivery,
          address: this.form.address
        },
        payment: {
          id: this.form.paymentId,
          currency: 'R01020A'
        },
        manager: 'avakymov21@gmail.com',
      }
    }

    $http(config)
      .then(this.handleSuccess.bind(this), this.handleError.bind(this))
  }

  this.handleSuccess = function() {
    this.success = true;
  }

  this.handleError = function(response) {
    this.error = true;
    this.errorMsg = response.data.message;
  }

})