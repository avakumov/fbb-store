var FtpDeploy = require('ftp-deploy');
 var fs = require('fs');

var config;

fs.readFile('.ftpauth', function(err, data) {
  config = JSON.parse(data);
  uploadFile();
})

/*var config = {
    username: "username",
    password: "password", // optional, prompted if none given 
    host: "ftp.someserver.com",
    port: 21,
    localRoot: __dirname + "/local-folder",
    remoteRoot: "/public_html/remote-folder/",
    exclude: ['.git', '.idea', 'tmp/*']
}*/


var uploadFile = function() {

  var ftpDeploy = new FtpDeploy();
  ftpDeploy.deploy(config, function(err) {
      if (err) console.log(err)
      else console.log('finished');
  });

  ftpDeploy.on('upload-error', function (data) {
    console.log(data.err); // data will also include filename, relativePath, and other goodies
  });
}
